Po raz pierwszy szyfrogramy zakodowane przy pomocy Enigmy udało się rozszyfrować
polskim kryptologom w grudniu 1932 roku w Pałacu Saskim w Warszawie, mieszczącym
siedzibę Biura Szyfrów Oddziału II Sztabu Głównego Wojska Polskiego. Prace Polaków,
głównie Mariana Rejewskiego, Jerzego Różyckiego i Henryka Zygalskiego pozwoliły na
dalsze prace nad dekodowaniem szyfrów stale unowocześnianych maszyn Enigma najpierw
w Polsce, a po wybuchu II wojny światowej we Francji i Wielkiej Brytanii.

Najczęściej odszyfrowywanymi wiadomościami były przekazy zaszyfrowane Enigmą w wersji
Wehrmachtu (Wehrmacht Enigma). Brytyjski wywiad wojskowy oznaczył Enigmę kryptonimem
ULTRA. Nazwa ta powstała ze względu na najwyższy stopień utajnienia faktu złamania
szyfru Enigmy, wyższy niż najtajniejszy (ang. Most Secret), czyli Ultratajny. 

Źródło: https://pl.wikipedia.org/wiki/Enigma

